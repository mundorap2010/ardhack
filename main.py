import time

from Arduino.ardhack import Arduino

a = Arduino(serial_port='/dev/ttyACM0')
time.sleep(3)
a.set_pin_mode(6, 'O')
time.sleep(1)
a.digital_write(6, 1)
print(a.digital_read(6))
time.sleep(5)
a.digital_write(6, 0) # Load status of LED 1 on - 0 off
print(a.digital_read(6))
